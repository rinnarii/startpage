var user = "アノンさん"; // replace this with your desired name

function start() {
    Clock();
    Img();
    Datum();
}
function Clock(){
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    m = leadingZero(m);
    h = leadingZero(h);
    document.getElementById('time').innerHTML = h + ":" + m;
    var t = setTimeout(Clock, 500);
}
function leadingZero(i) {
    if (i < 10) {i = "0" + i};
    return i;
}

function Img(){
    var today = new Date();
    var h = today.getHours();
    console.log(h);
	var morning = "おはよう";
	var day = "良い一日";
	var afternoon = "こんにちは";
	var evening = "こんばんは";

// I recommend replacing those hours on how the sun is at morning, noon, afternoon, and night, respectively.
    if(h>=6 && h<=11){
        document.getElementById('img').style.backgroundImage = "url('img/morning2.jpg')";
        document.getElementById('img').style.backgroundPositionY = "-210px";
        document.getElementById('welcome').innerHTML = morning + "、" + user + "！";
    }
    if(h>=12 && h<=15) {
        document.getElementById('img').style.backgroundImage = "url('img/day1.jpg')";
        document.getElementById('img').style.backgroundPositionY = "-250px";
        document.getElementById('welcome').innerHTML = day + "、" + user + "！";
    }
    if(h>=16 && h<=18) {
        document.getElementById('img').style.backgroundImage = "url('img/noon1.jpg')";
        document.getElementById('img').style.backgroundPositionY = "-150px";
        document.getElementById('welcome').innerHTML = afternoon + "、" + user + "！";
    }
    if((h>=19 && h<=23) || (h>=0 && h<=5)){
        document.getElementById('img').style.backgroundImage = "url('img/night1.jpg')";
        document.getElementById('img').style.backgroundPositionY = "-240px";
        document.getElementById('welcome').innerHTML = evening + "、" + user + "！";
    }
}
function Datum(){
    var today = new Date();
    var y = today.getUTCFullYear();

    var weekday = new Array(7);
    weekday[0]=  "日";
    weekday[1] = "月";
    weekday[2] = "火";
    weekday[3] = "水";
    weekday[4] = "木";
    weekday[5] = "金";
    weekday[6] = "土";

    var day = weekday[today.getDay()];

    var d = today.getDate();

    var m = today.getUTCMonth() + 1;

    var total = y + "年" + m + "月" + d + "日<br>" + day + "曜日";
    console.log(total);

    document.getElementById('datum').innerHTML = total;
}

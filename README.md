# Homepage
![preview](preview.png)

[Online version](https://rinnarii.gitlab.io/startpage)

A simple home page I customized according to my preference. 

[Code mostly taken from here](https://github.com/startpages/startpages.github.io/tree/master/startpages/nielsvanrijn_Dark-Startpage)

## Installation
Clone this repository and edit the files as you desire.
Here are the following files that requires modification:

### `startpage.html`
Replace the default links with the websites you want using this template:
```
<li><a class="sliding-middle-out" href="https://WEBSITE.com/" target="_blank"></a></li>
```

### `style.css`
To replace the default background, place an image file in the `img` directory and modify
```
background-image: url('img/cirno.jpg');
```

If you otherwise want a plain background instead, you can comment out the following lines by adding
```
/*	
background: no-repeat center center fixed;
background-size: cover;
background-image: url('img/cirno.jpg');
*/
```

### `js.js`
Change the variable value of `var user = "YOUR_NAME";`
